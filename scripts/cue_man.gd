extends Node

class_name CueMan

var subscriptions = []
var song: SongFile = null
var bpm = 140.0
var player: AudioStreamPlayer
var game_type_table: Dictionary = preload("res://game_type_table.tres").game_types
var latency: float = 0

func init(song_file: SongFile):
	song = song_file
	song.init()
	subscriptions = []
	get_tree().change_scene(game_type_table[song.game_id])
	player = AudioStreamPlayer.new()
	player.stream = song.song
	player.play()
	add_child(player)

func stop():
	player.remove_and_skip()
	song = null

func subscribe(caller_: Object, instrument_: String, offset_: float, callback_name_: String):
	subscriptions.append({
		"caller": caller_,
		"instrument": instrument_,
		"offset": offset_,
		"callback_name": callback_name_
	})
	pass

func _process(delta):
	if song != null:
		var timer = player.get_playback_position() + latency
		for subscriber in subscriptions:
			for cue in song.cues:
				if cue.instrument == subscriber.instrument and (not cue.fired) and cue.time < timer + (subscriber.offset * 60 / bpm):
					subscriber.caller.callv(subscriber.callback_name, [cue.note])
					cue.fired = true
