extends Resource


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

class_name SongFile

export var json_path: String
export var song_path: String

var data
var song: AudioStreamSample
var game_id: String
var cues: Array

func init():
	var file = File.new()
	file.open(json_path, File.READ)
	var text = file.get_as_text()
	var parse = JSON.parse(text)
	data = parse.result
	file.close()
	game_id = data.game_id
	cues = data.cues
	reset()
	song = load(song_path)

func reset():
	for cue in data.cues:
		cue.fired = false
