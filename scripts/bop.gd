extends Sprite


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export var subscribed = "beat"

onready var cue_man: CueMan = get_node("/root/cue_man")

func _ready():
	cue_man.subscribe(self, subscribed, 0.5, "beat")

func beat(note):
	$AnimationPlayer.playback_speed = cue_man.bpm / 60
	$AnimationPlayer.stop()
	$AnimationPlayer.play("bounce")
